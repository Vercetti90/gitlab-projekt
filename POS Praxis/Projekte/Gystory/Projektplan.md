# Projekt: Gystory 2.0

<br>
Implementierung der erlernten Konzepte aus dem 2.Semester.
<br>
<br>


### TO-DO-Liste:

- [X] Start
- [X] Vererbung (extends)
- [X] Polymorphie
- [X] Abstrakte Klassen (abstract)
- [X] Interfaces (implements)
- [X] Sammlungsklassen
- [X] Exception-Handling
- [X] Implementierung komplett
- [X] Design
- Erweiterungen: 
  - [X] Kalorienzähler
  - [X] Ausbau der Übungen 
- Optional
  - [X] GUI
  - [X] Training in Datei/Text speichern



### DEADLINE:  <span style="color:red"> **2021-06-15** </span>

<br>

### Ausgangssituation



![Bild 1](./Images/Klassendiagramm_Ausganssituation.jpg)

<br>
 
### Ideen & Überlegungen


![Bild 2](./Images/Überlegung.jpg)

<br>


### Stand 2021-05-18

![Bild 4](./Images/20210518_Stand.jpg)



