public abstract class Exercise {

    protected SelectExercise name;
    private double gewicht;
    private int anzahlDurchlaeufe;
    private int anzahlWiederholungen;
    private double dauer;
    private double startZeit;
    private double stopZeit;
    protected static double dauerExercisesGesamt=0.0;
    private String inputUser;

    /**
     *Superklasse Exercise
     */
    public Exercise(SelectExercise name) {
        this.name = name;
    }
    /**
     * Durchführung
     *
     * exerciseDurchführen-Methode
     * @param mitGewicht: true für die EnduranceExercise-Klasse & PowerExercise-Klasse / false für die WarmupExercise
     */
    public void exerciseDurchfuehren(boolean mitGewicht) {
        System.out.println(name);
        startZeit(false);
        saetzeText();
        eingabeText();
        this.anzahlDurchlaeufe = Input.checkInt("", "[0-9]+");
        for (int i = 1; i <= anzahlDurchlaeufe; i++) {
            wiederholungText(i);
            eingabeText();
            this.anzahlWiederholungen += Input.checkInt("", "[0-9]+");
            if(mitGewicht) {
                gewichtText(i);
                eingabeText();
                this.gewicht += Input.checkDouble("", "[0-9]+");
                System.out.println("\n");
            }
        }
        stopZeit(false);
    }

    /**
     * Zeitmessung
     *
     * startZeit-Methode
     * @param warmupExercise: true für die WarmupExercise / false für die EnduranceExercise-Klasse & PowerExercise-Klasse
     */
    public void startZeit(boolean warmupExercise){
        if(warmupExercise){
            inputUser = Input.checkStr(zeitStart(), "go|GO").toLowerCase();
            if (inputUser.equals("go")) {
                this.startZeit = System.currentTimeMillis();
                Text.zeilenumbruch();
                System.out.println(zeitLosGehts());
                Text.zeilenumbruch();
            }
        }else {
            this.startZeit = System.currentTimeMillis();
        }
    }
    /**
     * Zeitmessung
     * stopZeit-Methode
     * @param warmupExercise: true für die WarmupExercise / false für die EnduranceExercise-Klasse & PowerExercise-Klasse
     */
    public void stopZeit(boolean warmupExercise){
        if(warmupExercise){
            inputUser = Input.checkStr(zeitStop(), "end|END").toLowerCase();
            if (inputUser.equals("end")) {
                this.stopZeit = System.currentTimeMillis();
                this.dauer = (this.stopZeit - this.startZeit) / 1000.0;
                this.dauer = Math.round(this.dauer * 100.0) / 100.0;
                Text.zeilenumbruch();
             }
        }else{
            this.stopZeit = System.currentTimeMillis();
            this.dauer = (this.stopZeit - this.startZeit) / 1000.0;
            this.dauer = Math.round(this.dauer * 100.0) / 100.0;
            dauerExercisesGesamt = dauerExercisesGesamt + this.dauer;
            Text.zeilenumbruch();
        }
    }

    @Override
    public String toString() {
        return "Exercise " +"\n"
                + "Bezeichnung: "+ this.name +"\n"
                + "Dauer der Übung: "+this.getDauer()+" Sekunden"+"\n"
                ;
    }

    /**
     * Gibt die Dauer in Sekunden zurück
     * @return dauer
     */
    public double getDauer() {
        return this.dauer;
    }
    public String dauerInMinutenText() {
        return this.getDauerInMinuten() + " Minuten";
    }
    public double getDauerInMinuten(){
        return ((dauer/60.0)/100)*100;
    }
    public double getDauerInStunden(){
        return (dauer/3600.0);
    }

    public static double getDauerExercisesGesamt() {
        return dauerExercisesGesamt;
    }

    public void setDauer(double dauer) {
        this.dauer = dauer;
    }

    public double getStartZeit() {
        return startZeit;
    }

    public void setStartZeit(double startZeit) {
        this.startZeit = startZeit;
    }

    public double getStopZeit() {
        return stopZeit;
    }

    public void setStopZeit(double stopZeit) {
        this.stopZeit = stopZeit;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    public int getAnzahlDurchlaeufe() {
        return anzahlDurchlaeufe;
    }

    public void setAnzahlDurchlaeufe(int anzahlDurchlaeufe) {
        this.anzahlDurchlaeufe = anzahlDurchlaeufe;
    }

    public int getAnzahlWiederholungen() {
        return anzahlWiederholungen;
    }

    public void setAnzahlWiederholungen(int anzahlWiederholungen) {
        this.anzahlWiederholungen = anzahlWiederholungen;
    }

    /**
     * Texte für die Exercise-Klasse
     */
    public void saetzeText() {
        System.out.println("Wie viele Sätze hast du gemacht?");
    }
    public static void eingabeText() {
        System.out.print("Ihre Eingabe: ");
    }
    public void wiederholungText(int i) {
        System.out.println("Wie viele Wiederholungen hast du im " + i + ".Satz absolviert?");
    }
    public void gewichtText(int i) {
        System.out.println("Wie viel Gewicht hast du im " + i + ".Satz geschaft?");
    }


    //Texte für die Zeit-Methode
    public static String zeitStart() {
        return "Bist du bereit dein Training zu starten? Dann gebe \"go/GO\" ein, um die Zeitmessung zu beginnen!\nIhre Eingabe: ";
    }

    public static String zeitStop() {
        return "Sobald du fertig bist, gebe \"end/END\" ein, um die Zeitmessung zu stoppen!\nIhre Eingabe: ";
    }

    public static String zeitLosGehts() {
        return "Los gehts! Deine Zeit wird nun gemessen!";
    }
}
