/**
 *Subklasse PowerExercise
 *
 * Zuständig für Kraft-Übungen mit Gewicht
 *
 * Eine Stunde Krafttraining verbrennt ca. 480 Kalorien
 */

public class PowerExercise extends Exercise{


    private String kategorie = "KRAFT";
    private double kalorienverbrauchProStunde = 480.0;

    public PowerExercise(SelectExercise name) {
        super(name);
    }

    @Override
    public String toString() {
        return super.toString()
                + "Kategorie: "+ this.kategorie +"\n"
                + "Gewicht Gesamt: "+ getGewicht() +" kg"+"\n"
                + "Anzahl der Sätze: "+ getAnzahlDurchlaeufe() + "\n"
                + "Wiederholungen Gesamt: "+ getAnzahlWiederholungen() +"\n"
                + "Kalorienverbrauch: "+ Math.round(getKalorienverbrauchProStunde()*getDauerInStunden()*100/100)+" kcal";
    }

    public double getKalorienverbrauchProStunde() {
        return kalorienverbrauchProStunde;
    }

    public void setKalorienverbrauchProStunde(double kalorienverbrauchProStunde) {
        this.kalorienverbrauchProStunde = kalorienverbrauchProStunde;
    }

    public String getKategorie() {
        return kategorie;
    }
}
