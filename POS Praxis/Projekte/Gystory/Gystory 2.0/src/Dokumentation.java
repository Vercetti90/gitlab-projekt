import java.util.ArrayList;

public class Dokumentation
{
    //Datenfelder
    private int anzahlExercisesGesamt;
    private ArrayList<Exercise> dokumentation;

    //Konstruktor
    public Dokumentation(){

        this.dokumentation = new ArrayList<>();
    }

    //Methoden
    public void dokumentationHinzufuegen(Exercise exercise) {
        this.dokumentation.add(exercise);
        anzahlExercisesGesamt++;
    }
    public void alleUebungenAusgeben() {
        for (Exercise exercise : dokumentation) {
            System.out.println("--------------------------------------------");
            System.out.println(exercise.toString());                           //Polymorpher Methodenaufruf des dynamischen Typs
        }
    }

    public int getAnzahlExercisesGesamt() {
        return anzahlExercisesGesamt;
    }
}