public class Grundumsatz implements KoerperMassIndex{

    private double grundumsatz;

    /**
     * Harris-Benedict-Formel
     *
     * Der Grundumsatz beschreibt diejenige Energiemenge, die für die Aufrechterhaltung der lebensnotwendigen Körperfunktionen erforderlich ist.
     *
     * Frauen: Grundumsatz = 665,1 + (9,6 x Körpergewicht in Kilogramm) + (1,8 x Körpergröße in Centimeter) – (4,78 x Alter)
     * Männer: Grundumsatz = 66,47 + (13,7 x Körpergewicht in Kilogramm) + (5 x Körpergröße in Centimeter) – (6,8 x Alter)
     *
     */

    @Override
    public double berechnen(String geschlecht, int groesse, double gewicht, int alter) {
        if(geschlecht.equals("weiblich")) {
            this.grundumsatz = (665.1 + (9.6 * gewicht) + (1.8 * groesse) - (4.78 * alter));
            return grundumsatz;
        }else {
            this.grundumsatz = (66.47 + (13.7 * gewicht) + (5 * groesse) - (6.8 * alter));
            return grundumsatz;
        }
    }

    @Override
    public String toString() {
        return "Grundumsatz" +"\n"+
                "Ihr täglicher Grundumsatz beträgt momentan: " + this.grundumsatz +" kcal pro Tag";
    }
}
