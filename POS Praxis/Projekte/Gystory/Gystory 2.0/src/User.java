public class User {

    private String geschlecht;
    private String name;
    private int alter;
    private int groesse;
    private double gewicht;
    private boolean angelegt=false;


    public User() {

    }
    public void userAnlegen(){
        if(!angelegt){
            profilText();
            this.geschlecht = Input.checkStr(eingabeGeschlechtText(), "\\b([m|M|w|W\\s]+)");
            this.name = Input.checkStr(eingabeNameText(), "\\b([A-Za-z\\s]+)");
            this.alter = Input.checkInt(eingabeAlterText(), "[0-9]+");
            this.groesse = Input.checkInt(eingabeGroeßeText(), "[0-9]+");
            this.gewicht = Input.checkDouble(eingabeGewichtText(), "[0-9]*[.]*[0-9]+");
            System.out.println(profilErfolgreichErstellt());
            angelegt = true;
        }
    }

    @Override
    public String toString() {
        return "User" +"\n"+
                "Name: " + this.name + "\n" +
                "Geschlecht: " + this.getGeschlecht() + "\n" +
                "Alter: " + this.alter+" Jahre" + "\n"+
                "Größe (in cm): " + this.groesse +"\n"+
                "Gewicht: " + this.gewicht +" kg"+"\n"
                ;
    }

    public String getGeschlecht() {
        if(this.geschlecht.equals("m")){
            return "männlich";
        }else {
            return "weiblich";
        }
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    public int getGroesse() {
        return groesse;
    }

    public void setGroesse(int groesse) {
        this.groesse = groesse;
    }

    /**
     * Texte für die Nutzer-Klasse
     */
    public void profilText() {
        Text.zeilenumbruch();
        System.out.println("  ___          __ _ _             _                   \n"
                + " | _ \\_ _ ___ / _(_) |  __ _ _ _ | |___ __ _ ___ _ _  \n"
                + " |  _/ '_/ _ \\  _| | | / _` | ' \\| / -_) _` / -_) ' \\ \n"
                + " |_| |_| \\___/_| |_|_| \\__,_|_||_|_\\___\\__, \\___|_||_|\n"
                + "                                       |___/          \n"
                + "");
        Text.strichlinie();
        Text.zeilenumbruch();
    }
    public String profilErfolgreichErstellt(){
        Text.zeilenumbruch();
        return "Ihr Profil wurde erfolgreich angelegt!";
    }

    public String eingabeGeschlechtText() {
        return "Bitte geben Sie ihr Geschlecht ein (w/m): ";
    }

    public String eingabeNameText() {
        return "Bitte geben Sie ihren Namen ein: ";
    }

    public String eingabeAlterText() {
        return "Bitte geben Sie ihr Alter ein: ";
    }

    public String eingabeGewichtText() {
        return "Bitte geben Sie ihr Gewicht in Kilogramm ein: ";
    }
    public String eingabeGroeßeText() {
        return "Bitte geben Sie ihre Größe in Centimeter ein: ";
    }
}
