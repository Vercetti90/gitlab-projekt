import java.util.HashMap;
/**
 *Subklasse WarmupExercise
 *
 * Zuständig für Dehn-Übungen
 *
 * Eine Stunde Dehnen/Aufwärmen verbrennt zwischen 100 und 180 Kalorien
 */

public class WarmupExercise extends Exercise{

    private String kategorie = "AUFWÄRMEN / DEHNEN";
    private HashMap<SelectExercise,String> beschreibung;
    private double kalorienverbrauchProStunde = 140.0;

    public WarmupExercise(SelectExercise name) {
        super(name);
        System.out.println(name);
        this.beschreibung = new HashMap<>();
        beschreibungInit();
    }

    @Override
    public String toString() {
        return super.toString()
                + "Kategorie: "+ this.kategorie +"\n"
                + "Kalorienverbrauch: "+ Math.round(getKalorienverbrauchProStunde()*getDauerInStunden()*100/100)+" kcal";
    }

    /**
     * Befüllen der HashMap mit den Beschreibungen der einzelnen WarmupExercises!
     */
    private void beschreibungInit() {
        beschreibung.put(SelectExercise.ADDUKTOREN_1, beschreibungAdduktoren1());
        beschreibung.put(SelectExercise.ADDUKTOREN_2, beschreibungAdduktoren2());
        beschreibung.put(SelectExercise.KAPUZENMUSKEL, beschreibungKapuzenmuskelDehnen());
        beschreibung.put(SelectExercise.BRUSTMUSKEL_DEHNEN, beschreibungBrustmukelDehnen());
        beschreibung.put(SelectExercise.OBERSCHENKEL_VORDERSEITE, beschreibungOberschenkelVorderseiteDehnen());
        beschreibung.put(SelectExercise.SEITLICHE_BAUCHMUSKULATUR, beschreibungSeitlicheBauchmuskulaturDehnen());
        beschreibung.put(SelectExercise.TRIZEPS, beschreibungTrizepsDehnen());
        beschreibung.put(SelectExercise.UNTEREN_RUECKEN, beschreibungUnterenRueckenDehnen());
        beschreibung.put(SelectExercise.WADEN, beschreibungWadenDehnen());
        beschreibung.put(SelectExercise.WIRBELSAEULE, beschreibungWirbelsaeuleDehnen());
        System.out.println(beschreibung.get(name));
    }

    public double getKalorienverbrauchProStunde() {
        return kalorienverbrauchProStunde;
    }

    public void setKalorienverbrauchProStunde(double kalorienverbrauchProStunde) {
        this.kalorienverbrauchProStunde = kalorienverbrauchProStunde;
    }

    /**
     * Text für die Beschreibung der WarmupExercise
     */
    public String beschreibungBrustmukelDehnen(){
        return "Anleitung: "+"\n"+
                "Du stehst aufrecht in Schrittstellung an einer Wand und stützt mit lang gestrecktem Arm eine Hand daran ab.\n"
                + "Drehe den Oberkörper in die entgegen gesetzte Richtung und lehne dich dabei leicht nach vorne.\n"
                + "Je nach Handposition - höher oder tiefer - dehnst du unterschiedliche Anteile des Brustmuskels.";
    }
    public String beschreibungKapuzenmuskelDehnen(){
        return "Anleitung: "+"\n"+
                "Stelle dich hüftbreit auf und verhake die Hände ineinander, die Handflächen zeigen nach außen.\n"
                + "Strecke die Arme über den Kopf und ziehe sie nach vorne, neige den Kopf leicht nach unten.\n"
                + "Du spürst den Stretch bis in die Schulterblätter."
                +"\n";
    }
    public String beschreibungTrizepsDehnen(){
        return "Anleitung: "+"\n"+
                "Du stehst hüftbreit, beugst einen Arm und legst die Hand zwischen den Schulterblättern ab.\n"
                + "Die andere Hand greift zum Ellenbogengelenk und zieht sanft nach."
                +"\n";
    }
    public String beschreibungUnterenRueckenDehnen(){
        return "Anleitung: "+"\n"+
                "Gehe für die \"Katze-Kuh-Dehnung\" in den Vierfüßlerstand, wobei die Knie schulterbreit auseinander stehen.\n"
                + "Wölbe deinen Rücken nach oben (Katze) und lass den Oberkörper anschließend Richtung Boden herab sinken (Kuh),"
                +" wobei du in ein leichtes Hohlkreuz gehst"
                +"\n";
    }
    public String beschreibungWirbelsaeuleDehnen(){
        return "Anleitung: "+"\n"+
                "Lege dich auf den Rücken, ein Bein ist im Hüft- und Kniegelenk etwa 90 Grad angewinkelt.\n"
                + "Greife mit der anderen Hand das Knie und lege es auf der gegenüberliegenden Seite ab.\n"
                + "Beide Schultern bleiben am Boden."
                +"\n";
    }
    public String beschreibungSeitlicheBauchmuskulaturDehnen(){
        return "Anleitung: "+"\n"+
                "Stelle dich etwas weiter als Hüftbreit auf und lehne dich zu einer Seite nach unten, so dass du die Hand am Knöchel ablegen kannst.\n"
                + "Der andere Arm zeigt getreckt Richtung Decke, dein Blick folgt dieser Bewegung."
                +"\n";
    }
    public String beschreibungWadenDehnen(){
        return "Anleitung: "+"\n"+
                "Gehe in Schrittstellung und stütze dich mit beiden Armen an einer Wand oder einem Geländer ab.\n"
                + "Das hintere Bein ist gestreckt, das vordere gebeugt.\n"
                + "Lehne dich nach vorne, so dass du den Stretch in der Wade spürst"
                +"\n";
    }
    public String beschreibungOberschenkelVorderseiteDehnen(){
        return "Anleitung: "+"\n"+
                "Du stehst aufrecht und greifst mit einer Hand dein Fußgelenk.\n"
                + "Ziehe die Ferse Richtung Po.\n"
                + "Beide Knie sind auf einer Höhe."
                +"\n";
    }
    public String beschreibungAdduktoren1(){
        return "Anleitung: "+"\n"+
                "Setze dich aufrecht hin, die Beine sind angewinkelt und die Fußsohlen liegen aneinander.\n"
                + "Du kannst sie mit deinen Händen umfassen.\n"
                + "Lasse die Knie jetzt nach außen fallen.\n"
                + "Die Position wird auch \"Schmetterlingssitz\" genannt."
                +"\n";
    }
    public String beschreibungAdduktoren2(){
        return "Anleitung: "+"\n"+
                "Du sitzt aufrecht, ein Bein ist gestreckt, das andere gebeugt, zum Oberkörper heran gezogen und über das gestreckt Bein geschlagen.\n"
                + "Stelle den Fuß auf Kniehöhe ab und umfasse das gebeugte Bein mit dem gegenüberliegenden Arm.\n"
                + "Stütze dich mit der anderen Hand ab und drehe dich leicht ein."
                +"\n";
    }
}
