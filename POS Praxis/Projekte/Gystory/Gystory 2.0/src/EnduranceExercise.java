/**
 *Subklasse EnduranceExercise
 *
 * Zuständig für Ausdauer-Kraft-Übungen ohne Gewicht
 *
 * Eine Stunde Krafttraining mit Eigengewicht verbrennt ca. 320 Kalorien
 */

public class EnduranceExercise extends Exercise{

    private String kategorie = "AUSDAUER";
    private double kalorienverbrauchProStunde = 320.0;

    public EnduranceExercise(SelectExercise name) {
        super(name);
    }


    @Override
    public String toString() {
        return super.toString()
                + "Kategorie: "+ this.kategorie +"\n"
                + "Anzahl der Sätze: "+ this.getAnzahlDurchlaeufe() + "\n"
                + "Wiederholungen Gesamt: "+ this.getAnzahlWiederholungen() +"\n"
                + "Kalorienverbrauch: "+ Math.round(getKalorienverbrauchProStunde()*getDauerInStunden()*100/100)+" kcal";
    }

    public double getKalorienverbrauchProStunde() {
        return kalorienverbrauchProStunde;
    }

    public void setKalorienverbrauchProStunde(double kalorienverbrauchProStunde) {
        this.kalorienverbrauchProStunde = kalorienverbrauchProStunde;
    }
    public String getKategorie() {
        return kategorie;
    }
}
