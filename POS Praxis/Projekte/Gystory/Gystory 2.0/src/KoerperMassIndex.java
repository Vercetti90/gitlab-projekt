public interface KoerperMassIndex {

    double berechnen(String geschlecht, int groesse, double gewicht, int alter);

}
