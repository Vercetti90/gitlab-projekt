import java.util.Scanner;

public class Input {
    /*
     *Input-Klasse = Diese Klasse liest den Input des Users ein,
     * parst ihn und kontrolliert ihn auf Übereinstimmung mit dem angegebenen Regex
     */
    private static Scanner scan = new Scanner(System.in);

    public static int checkInt(String text, String regex) {
        return Integer.parseInt(Input.checkString(text, regex));
    }

    public static double checkDouble(String text, String regex) {
        return Double.parseDouble(Input.checkString(text, regex));
    }

    public static String checkStr(String text, String regex) {
        return Input.checkString(text, regex);
    }

    public static String checkString(String text, String regex) {
        String checkStr;
        do {
            System.out.print(text);
            checkStr = scan.nextLine();
        } while (!(checkStr.matches(regex)));
        return checkStr;
    }
}