public interface KoerperMassAusgabe {

    Gewichtskategorie ausgabe(String geschlecht, double ergebnis);

}
