public class Text {

    public static void logo() {
        System.out.println("                                                                                         \n" +
                "                                                                                         \n" +
                "  .g8\"\"\"bgd                   mm                                                         \n" +
                ".dP'     `M                   MM                                                         \n" +
                "dM'       `7M'   `MF,pP\"Ybd mmMMmm ,pW\"Wq.`7Mb,od8 `7M'   `MF'     pd*\"*b.     ,pP\"\"Yq.  \n" +
                "MM          VA   ,V 8I   `\"   MM  6W'   `Wb MM' \"'   VA   ,V      (O)   j8    6W'    `Wb \n" +
                "MM.    `7MMF'VA ,V  `YMMMa.   MM  8M     M8 MM        VA ,V           ,;j9    8M      M8 \n" +
                "`Mb.     MM   VVV   L.   I8   MM  YA.   ,A9 MM         VVV         ,-='    ,, YA.    ,A9 \n" +
                "  `\"bmmmdPY   ,V    M9mmmP'   `Mbmo`Ybmd9'.JMML.       ,V         Ammmmmmm db  `Ybmmd9'  \n" +
                "             ,V                                       ,V                                 \n" +
                "          OOb\"                                     OOb\"                                  \n" +
                "\n");
    }

    public static void titel() {
        logo();
        Text.zeilenumbruch();
        System.out.println("\t\t\t\t\t\t...deine persönliche Fitness Dokumentation");
        Text.zeilenumbruch();
        Text.zeilenumbruch();

    }

    public static void ende() {

        Text.zeilenumbruch();
        System.out.println("\t\t\t\t\t\t\t...good Rest Day");
        Text.zeilenumbruch();
        logo();
    }

    public static void hauptmenueLogo() {
        System.out.println("  _  _                _                 _  _ \n"
                + " | || |__ _ _  _ _ __| |_ _ __  ___ _ _(_)(_)\n"
                + " | __ / _` | || | '_ \\  _| '  \\/ -_) ' \\ || |\n"
                + " |_||_\\__,_|\\_,_| .__/\\__|_|_|_\\___|_||_\\_,_|\n"
                + "                |_|                          \n"
                + "");

    }
    //Texte für Layout Zeilenumbruch + Striche

    public static void zeilenumbruch() {
        System.out.print("\n");
    }

    public static void strichlinie() {
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }

}
