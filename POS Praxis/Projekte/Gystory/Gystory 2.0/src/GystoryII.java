public class GystoryII {

    public static void main(String[] args) {

        //DERZEIT ZUM TESTEN
        Text.titel();
        //USER TEST
        User user = new User();
        user.userAnlegen();
        Text.zeilenumbruch();
        System.out.println(user.toString());


        // POWEREXERCISE TEST

        Exercise exercise = new PowerExercise(SelectExercise.BIZEPSCURLS);
        exercise.exerciseDurchfuehren(true);
        System.out.println(exercise.toString());
        Text.strichlinie();

        //ENDURANCEEXERCISE TEST
        Exercise exercise1= new EnduranceExercise(SelectExercise.LIEGESTUETZE);
        exercise1.exerciseDurchfuehren(false);
        System.out.println(exercise1.toString());
        Text.strichlinie();

        Exercise exercise2= new EnduranceExercise(SelectExercise.KLIMMZUEGE);
        exercise2.exerciseDurchfuehren(false);
        System.out.println(exercise2.toString());
        Text.strichlinie();

        //WARUMUPEXERCISE TEST
        Exercise exercise3 = new WarmupExercise(SelectExercise.KAPUZENMUSKEL);
        exercise3.startZeit(true);
        exercise3.stopZeit(true);
        System.out.println(exercise3.toString());
        Text.strichlinie();

        //TEST INTERFACE KÖRPERMASSINDEX
        KoerperMassIndex koerperMassIndex;
        KoerperMassAusgabe koerperMassAusgabe;
        koerperMassIndex = new BMI();   //Für den Polymorphen-Aufruf
        koerperMassAusgabe = new BMI();   //Für den Polymorphen-Aufruf
        System.out.println(koerperMassIndex.berechnen(user.getGeschlecht(),175,77.0, user.getAlter()));
        System.out.println(koerperMassAusgabe.ausgabe("männlich", koerperMassIndex.berechnen(user.getGeschlecht(),175,77.0, user.getAlter())));
        System.out.println(koerperMassAusgabe.ausgabe("weiblich", koerperMassIndex.berechnen(user.getGeschlecht(),175,77.0, user.getAlter())));
        System.out.println(koerperMassIndex.toString());
        Text.strichlinie();
        koerperMassIndex = new Grundumsatz();
        System.out.println(koerperMassIndex.berechnen("weiblich",185,77.0,31));
        System.out.println(koerperMassIndex.berechnen("männlich",185,77.0,31));
        System.out.println(koerperMassIndex.toString());
        Text.strichlinie();
        System.out.println(user.toString());

        //TEST DOKUMENTATION EXERCISE
        Dokumentation doku = new Dokumentation();
        doku.dokumentationHinzufuegen(exercise);
        doku.dokumentationHinzufuegen(exercise1);
        doku.dokumentationHinzufuegen(exercise2);
        doku.dokumentationHinzufuegen(exercise3);
        doku.alleUebungenAusgeben();

        //TEST GESAMTAUSGABE DATEI (Anzahl der Übungen, Gesamttrainingszeit)
        Text.strichlinie();
        System.out.println("Gesamtanzahl an Exercises während einer Trainings-Einheit: "+doku.getAnzahlExercisesGesamt()+" Übungen");
        System.out.println("Gesamtzeit einer Trainings-Einheit: "+Math.round(((Exercise.dauerExercisesGesamt/100)*100)/60)+" Minuten");

    }
}
