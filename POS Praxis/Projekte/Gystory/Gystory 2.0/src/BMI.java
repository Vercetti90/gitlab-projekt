public class BMI implements KoerperMassIndex, KoerperMassAusgabe {

    private double bmi;
    private Gewichtskategorie gewichtskategorie;


    @Override
    public double berechnen(String geschlecht, int groesse, double gewicht, int alter) {
        this.bmi = groesse / 100.0;
        this.bmi = Math.round((gewicht / Math.pow(this.bmi, 2)) * 100.0) / 100.0;
        return this.bmi;
    }

    @Override
    public Gewichtskategorie ausgabe(String geschlecht,double bmi) {
        if(geschlecht.equals("männlich")){
            if (this.bmi < 20) {
                this.gewichtskategorie = Gewichtskategorie.UNTERGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi < 26) {
                this.gewichtskategorie = Gewichtskategorie.NORMALGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi < 31) {
                this.gewichtskategorie = Gewichtskategorie.UEBERGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi <= 40) {
                this.gewichtskategorie = Gewichtskategorie.ADIPOSITAS;
                return this.gewichtskategorie;
            } else {
                this.gewichtskategorie = Gewichtskategorie.STARKE_ADIPOSITAS;
                return this.gewichtskategorie;
            }
        }else{
            if (this.bmi < 19) {
                this.gewichtskategorie = Gewichtskategorie.UNTERGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi < 25) {
                this.gewichtskategorie = Gewichtskategorie.NORMALGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi < 31) {
                this.gewichtskategorie = Gewichtskategorie.UEBERGEWICHT;
                return this.gewichtskategorie;
            } else if (this.bmi <= 40) {
                this.gewichtskategorie = Gewichtskategorie.ADIPOSITAS;
                return this.gewichtskategorie;
            } else {
                this.gewichtskategorie = Gewichtskategorie.STARKE_ADIPOSITAS;
                return this.gewichtskategorie;
            }
        }
    }

    @Override
    public String toString() {
        return "Body-Mass-Index" +"\n"+
                "Ihr BMI beträgt momentan: " + getBmi()+"\n"
               +"Aus diesem Wert ergibt sich: " + getGewichtskategorie();

    }

    public double getBmi() {
        return bmi;
    }

    public Gewichtskategorie getGewichtskategorie() {
        return gewichtskategorie;
    }

}
