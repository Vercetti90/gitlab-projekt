# Überschrift
### Einleitung
<p>
---     Unterteilung

Tabelle:

|Technologie-Name | Verwendete Version                                          |
|-----------------|:------------------------------------------------------------|                                        
|Ubuntu           |Ubuntu 20.04 LTS                                             |
|crontab          |vixie-cron-4.1-81.el5.x86_64 				|
|ssh              |OpenSSH_8.2p1 Ubuntu-4ubuntu0.1, OpenSSL 1.1.1f  31 Mar 2020 |

CODE Highlighting z.B.:
```bash
if [ -d "$folderToBackup" ]; then
        echo "Verzeichnis $folderToBackup existiert"
```
oder

```java
System.out.println("Hello World");
```

Aufzählungen:

+ bash = Bash-Befehl
+ backupScript.bash = Name des Backup Skript mit der Endung .bash

Literaturangaben:


  * [https://de.wikipedia.org/wiki/Datensicherung](https://de.wikipedia.org/wiki/Datensicherung)
  * [https://de.wikipedia.org/wiki/7-Zip](https://de.wikipedia.org/wiki/7-Zip)
  * [https://wiki.ubuntuusers.de/Cron/](https://wiki.ubuntuusers.de/Cron/)