use dbi_w3c;

/* SQL Statement 1*/
SELECT
  COUNT(customers.CustomerID) AS AnzahlDerKunden,
  customers.Country
FROM
  customers
GROUP BY
  customers.Country
HAVING
  COUNT(customers.CustomerID) > 5;

/* SQL Statement 2*/  
SELECT
  COUNT(products.ProductID) as ProdukteAnzahl,
  suppliers.SupplierID,
  suppliers.SupplierName,
  suppliers.Country
FROM
  suppliers
  RIGHT JOIN products on products.SupplierID
GROUP BY
  suppliers.SupplierID
HAVING
  COUNT(products.ProductID) < 5;

/* SQL Statement 3*/
SELECT
  COUNT(products.ProductID) as ProdukteAnzahl,
  suppliers.SupplierID,
  suppliers.SupplierName,
  suppliers.Country
FROM
  suppliers
  INNER JOIN products on products.SupplierID
GROUP BY
  suppliers.SupplierID
HAVING
  COUNT(products.ProductID) > 5
ORDER BY
  suppliers.SupplierID ASC
LIMIT
  10;
SELECT
  shippers.ShipperName,
  COUNT(orders.OrderID) AS NumberOfOrders
FROM
  orders
  INNER JOIN shippers ON orders.ShipperID = shippers.ShipperID
WHERE
  shippers.shipperName IS NOT NULL
GROUP BY
  shippers.shipperName;

/* SQL Statement 4*/


/* SELECT
  orderdetails.OrderDetailID,
  SUM(quantityOrdered) AS itemsCount,
  SUM(priceeach * quantityOrdered) AS total
FROM
  orderdetails
GROUP BY
  ordernumber
HAVING
  total > 1000; */